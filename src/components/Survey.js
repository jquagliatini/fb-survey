import Vue from "vue";
import Button from "./system/Button";
import Icon from "./system/Icon";

import FbSurveyElement from "./SurveyElement.vue";

export default Vue.extend({
  name: "fb-survey",
  components: { Button, Icon },
  data() {
    return {
      current: 0,
      transition: "slide",
      canGoNext: true,
      lockReason: undefined,
      recap: {},
      formData: {},
    };
  },
  computed: {
    hasPrevious() {
      return (
        Array.isArray(this.$slots.default) &&
        this.$slots.default.length > 0 &&
        this.current > 0
      );
    },
    hasNext() {
      return (
        Array.isArray(this.$slots.default) &&
        this.$slots.default.length > 0 &&
        this.current < this.$slots.default.length - 1
      );
    },
    isLast() {
      return (
        Array.isArray(this.$slots.default) &&
        this.$slots.default.length > 0 &&
        this.current >= this.$slots.default.length - 1
      );
    },
    currentSlot() {
      return this.current >= this.$slots.default.length
        ? this.$createElement(FbSurveyElement, { scopedSlots: { answers: () => this.$createElement("div", Object.values(this.recap)) } })
        : this.$slots.default[this.current];
    },
  },
  created() {
    this.$on("validate", (d) => {
      this.canGoNext = d.isValid;
      this.lockReason = d.error;

      this.formData = {
        ...this.formData,
        [d.id]: d,
      };
      this.recap = {
        ...this.recap,
        [d.id]: d.recap,
      };
    });
  },
  beforeMount() {
    this.current = this.retrieveCurrentSlide();
  },
  methods: {
    retrieveCurrentSlide() {
      return Number(sessionStorage.getItem("SURVEY_SLIDE"));
    },
    setCurrentSlide(index) {
      let idx;
      if (isNaN((idx = Number(index)))) {
        throw new Error("Expected a number");
      }

      this.current = idx;
      sessionStorage.setItem("SURVEY_SLIDE", this.current);
    },
    next() {
      this.transition = "slide";
      this.setCurrentSlide(this.current + 1);
    },
    previous() {
      this.transition = "slide-rev";
      this.canGoNext = true;
      this.lockReason = undefined;
      this.setCurrentSlide(this.current - 1);
    },
  },
  render(h) {
    return (
      <div class="survey pt-40 w-1/3">
        <transition mode="out-in" name={this.transition}>
          {this.currentSlot}
        </transition>
        <aside class="block mt-5 flex justify-between">
          <Button class="disabled:text-gray-300 disabled:border-gray-300" disabled={!this.hasPrevious} onClick={this.previous}>
            <Icon name="arrow-backward-outline" />&nbsp;précédent
          </Button>
          {
            this.current < this.$slots.default.length &&
              (
                <Button class={
                    "text-white font-bold transition-colors duration-150 " +
                    (this.isLast
                      ? "bg-indigo-500 border-indigo-600 hover:bg-indigo-400 disabled:bg-indigo-100 disabled:border-indigo-200 disabled:text-gray-400"
                      : "bg-purple-500 border-purple-600 hover:bg-purple-600 disabled:bg-purple-100 disabled:border-purple-200 disabled:text-gray-400")
                  } onClick={this.next} disabled={!this.canGoNext} title={this.lockReason ? this.lockReason : undefined}
                >
                  {
                    this.isLast
                      ? [ h(Icon, { props: { name: "menu-2-outline" } }), " résumé" ]
                      : [ "suivant ", h(Icon, { props: { name: "arrow-forward-outline" } }) ]
                  }
                </Button>
              )
           }
        </aside>
      </div>
    )
  },
});
