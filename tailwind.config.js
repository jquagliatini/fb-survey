module.exports = {
  purge: ["./src/**/*.vue", "./src/**/*.js"],
  theme: {
    extend: {},
  },
  variants: {
    backgroundColor: ["responsive", "hover", "focus", "disabled"],
    borderColor: ["responsive", "hover", "focus", "disabled", "focus-within"],
    boxShadow: ["responsive", "hover", "focus", "focus-within"],
    width: ["responsive", "disabled"],
    textColor: ["responsive", "hover", "focus", "disabled"],
    margin: ["responsive", "disabled"],
  },
  plugins: [],
};
