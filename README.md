# fb Survey

A bit of fun with [Vue](https://vuejs.org),
[Vue Transitions](https://vuejs.org/v2/guide/transitions.html)
and [Tailwind CSS](https://tailwindcss.com)

![A Screenshot of the app](./screenshot.png)

<small>All icons, are provided by https://akveo.github.io/eva-icons</small>

## Roadmap

- [x] Gestion déclarative des questions et réponses.
- [x] Customisation de chaque question, et de leur formulaire respectif.
- [x] Intégration de la validation dans la navigation entre questions.
- [x] Ajout d'un étape récapitulative à la fin.
- [ ] Responsiveness.
- [ ] Conservation des saisies entre les navigations.
- [ ] Persistence des informations saisies.
